import psycopg2

from geojson import Feature, FeatureCollection, loads
from results import buffer, scores
from flask import  request
from vote import voteIncrement, info
from process import populate, addFidGeom
import json
from gsppy.gsp import GSP

class Postgis(object):
    def __init__(self, user, passw, port, host):
        self.connection = psycopg2.connect(database='gis', user=user, password=passw, port=port, host=host)
        self.cursor = self.connection.cursor()
        self.connection.autocommit = True


    def get_parking(self) -> FeatureCollection:
        """
        """
        parking = []
        self.cursor.execute("SELECT osm_id, ST_AsGeoJSON(ST_Transform(geom,4326)) AS geometry FROM parking;")
        for row in self.cursor.fetchall():
            parking.append(Feature(properties={'id': row[0]},
                                    geometry=loads(row[1])))
        return FeatureCollection(parking)
    
    def get_processed(self) -> FeatureCollection:
        parking_p = []
        self.cursor.execute("SELECT fid, ST_AsGeoJSON(ST_Transform(geom,4326)) AS geometry FROM parking_p;")
        for row in self.cursor.fetchall():
            parking_p.append(Feature(properties={'id': row[0]},
                                    geometry=loads(row[1])))
        return FeatureCollection(parking_p)
    
    def get_amenities(self) -> FeatureCollection:

        self.cursor.execute("SELECT osm_id_a, ST_AsGeoJSON(ST_Transform(geom,4326)) AS geometry FROM amenities;")
        amen = []  # type: list

        for row in self.cursor.fetchall():
            amen.append(Feature(properties={'id': row[0]},
                                    geometry=loads(row[1])))

        return FeatureCollection(amen)
    
    def get_trans(self) -> FeatureCollection:

        self.cursor.execute("SELECT osm_id_t, ST_AsGeoJSON(ST_Transform(geom,4326)) AS geometry FROM public_transport;")
        trans = []  # type: list

        for row in self.cursor.fetchall():
            trans.append(Feature(properties={'id': row[0]},
                                    geometry=loads(row[1])))

        return FeatureCollection(trans)

    def get_shops(self) -> FeatureCollection:

        self.cursor.execute("SELECT osm_id_s, ST_AsGeoJSON(ST_Transform(geom,4326)) AS geometry FROM shops;")
        shops = []  # type: list

        for row in self.cursor.fetchall():
            shops.append(Feature(properties={'id': row[0]},
                                    geometry=loads(row[1])))

        return FeatureCollection(shops)
    
    def processing(self):
        cursor = self.cursor
        cr_tmp = "CREATE TEMP TABLE tmp_table AS SELECT * FROM parking_p LIMIT 0;"
        cursor.execute(cr_tmp)
        Fid = request.args.get("id")
        dist = request.args.get("dist")
        output = buffer(cursor, Fid, dist)
        addFidGeom(cursor, Fid, dist)
        s_results = output.shop_results()
        t_results = output.tran_results()
        a_results = output.amen_results()
        v1 = "s_f"
        v2 = "t_f"
        v3 = "a_f"
        populate(cursor, Fid, v1, v2, v3, s_results, t_results, a_results)
        output_count = {}
        output_count['amen_found'] = a_results
        output_count['tran_found'] = t_results
        output_count['shop_found'] = s_results
        return (output_count)

    def scoresDisp(self):
        cursor = self.cursor
        Fid = request.args.get("id")
        sw = request.args.get("sw")
        tw = request.args.get("tw")
        aw = request.args.get("aw")
        scoreCalc = scores
        score = scoreCalc.final(cursor, tw, sw, aw, Fid)
        drop_sql = "drop table tmp_table"
        cursor.execute(drop_sql) 
        return score

    def infor(self):
        cursor = self.cursor
        Fidinfo = request.args.get("id")
        infor = info(cursor, Fidinfo)
        return infor

    def voteAdd(self):
        cursor = self.cursor
        Fidvote = request.args.get("id")
        typech = request.args.get("type")
        vote = voteIncrement(cursor, Fidvote, typech)
        return vote

    def clicklog(self):
        cursor = self.cursor
        data = request.get_data()
        data = data.decode('utf-8')
        sessid = request.args.get('sessid')
        cond = request.args.get('condition')
        clicklogsql = f"insert into clicklog (sessionid, condition, clickdata) values ('{sessid}', '{cond}', '{data}');"
        cursor.execute(clicklogsql)
        return("success")
    
    def inserteval(self, ssid, dataeval):
        cursor = self.cursor
        datae = json.dumps(dataeval)
        evalsql = f"insert into eval (ssid, data) values ('{ssid}', '{datae}');"
        cursor.execute(evalsql)
        return("success")

    def evalresultdisp(self, service):
        evalressql = "SELECT data, ssid, clickdata FROM evalresults"
        self.cursor.execute(evalressql)
        evalres = {}
        idlist = []
        q4 = []
        q5 = []
        q6 = []
        q7 = []
        spread_id = {your spreadsheet id}
        for row in self.cursor.fetchall():
            row1 = str(row[0]).replace("%20", " ")
            row2 = row1.replace("%2C", ",")
            row3 = json.loads(row2)
            evalres[row[1]] = row3
            id = str(row[1])
            proid = str(row3["proid"])
            q1 = str(row3["q1"])
            q2 = str(row3["q2"])
            q3 = str(row3["q3"])
            q4 = str(row3["q4"])
            q5 = str(row3["q5"])
            q6 = str(row3["q6"])
            q7 = str(row3["q7"])
            q8 = str(row3["q8"])
            q9 = str(row3["q9"])
            q10 = str(row3["q10"])
            q11 = str(row3["q11"])
            exp = str(row3["exptool"])
            gen = str(row3["gender"])
            age = str(row3["age"])
            ev = str(row3["ev"])
            body = {
                "range": "Sheet1",
                "majorDimension": "ROWS",
                "values": [[ev]]
            }
            range  = 'Sheet1'
            result = service.spreadsheets().values().append(spreadsheetId=spread_id,  valueInputOption="USER_ENTERED", range=range, body=body).execute()
        return(str(row))

    def clickresultdisp(self):
        clickressql = "SELECT clickdata FROM evalresults"
        self.cursor.execute(clickressql)
        clicklist = []
        evnames = []
        datacl1 = []
        row3 = []
        row4 = {}
        row6 = []
        row8 = {}
        for row in self.cursor.fetchall():
            rowjson = json.loads(row[0])
            clicklist.append(rowjson[0]["data"])
            #result = GSP(datacl1).search(0.3)
        for index, row1 in enumerate(clicklist):
            row4[index] = clicklist[index]
        for row5 in row4[49]:
            row6.append(row5["event_name"])
        return(str(row6))
