from geojson import Feature, FeatureCollection, loads
import json
def info(cursor, fid):
    infosql = f"SELECT fid, dist, a_v, s_v, t_v, a_f, s_f, t_f, score, vote FROM parking_p where fid = '{fid}';"
    cursor.execute(infosql)
    infop = []
    for row in cursor.fetchall():
        infop.append(Feature(properties={'fid': str(row[0]), 'dist': str(row[1]), 'a_v': str(row[2]), 's_v': str(row[3]), 't_v': str(row[4]), 'a_f': str(row[5]), 's_f': str(row[6]), 't_f': str(row[7]), 'score': str(row[8]), 'vote': str(row[9]) }))
    return json.dumps(infop)

def voteIncrement(cursor, fid, typech):
    sql = f"update parking_p set vote = vote + 1 where fid = '{fid}'"
    sqltype = f"insert into typech (fid, typech) values ('{fid}','{typech}')"
    cursor.execute(sql)
    cursor.execute(sqltype)
    return sql
