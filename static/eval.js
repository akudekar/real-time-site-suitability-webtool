$( "form" ).on( "submit", function( event ) {

	event.preventDefault();
	var ans = $( this ).serializeArray();
	$.ajax({
		url:"/que",
		type:"POST",
		data:ans,
		contentType:"application/json; charset=utf-8",
		dataType:"json",
	})
	Swal.fire({  
		title: 'You have submitted your session successfully.',
		html: '<p>Please click the following link to complete the study. You will be redirected to the Prolific app - </p><p><a href="https://app.prolific.co/submissions/complete?cc=26BB4ADB">https://app.prolific.co/submissions/complete?cc=26BB4ADB</a></p>',
		icon: 'success',  
	  });
  });
