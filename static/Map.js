var consent;

var voted = [];

var sessionid = Math.random().toString(36).substr(2, 20);

taskurl = "/tasks?ssid=" + sessionid

Swal.fire({  
    title: 'User study consent',
    html: '<p style="text-align: justify;">This is a usability study for scientific research purposes.. To find out more about GDPR rules for research purposes, please click <a href="https://www.privacy-regulation.eu/en/article-89-safeguards-and-derogations-relating-to-processing-for-archiving-purposes-the-public-interest-scientific-or-hi-GDPR.html"><b>HERE</b></a></p><p style="text-align: justify;">In this user study, we will collect/generate:- <ul style="text-align:left; padding-left:20px;"><li><b>User clicks and interactions-</b>When you click on a button or on other user interface services such as map interactions such as zoom-in/zoom-out, pan, etc.</li><li><b>A random session id - </b>A random session id will be generated for you whch is displayed on your task sheet.</li></ul></p><p style="text-align:justify;"><b>No personal information</b> linking to you is captured in this study. This data will be stored and analyzed for scientific purposes <b>only</b></p><p style="text-align:justify">To view <b>consent information and contact details</b>, click <a target="_blank" href="/consent"><b>HERE</b></a></p><p style="text-align:left">To view <b>statement from researchers</b> please click <a target="_blank" href="/statres"><b>HERE</b></a><p style="text-align:justify;"><b>As this is a usability study for academic research purposes, participants cannot opt-out of interaction tracking. If you wish to not let us capture this information, you can choose to leave the user study.</b></p></p>',   
    input: 'checkbox',
    inputPlaceholder: 'I confirm I have agreed to let researchers of this study capture my clicks and interactions.',
    confirmButtonText: `Accept`,
    width: '600px',
    allowOutsideClick: false,
    inputValidator: (result) => {
        return !result && 'You need to agree with T & C'
    }  
  }).then((result) => { 
      if (result.isConfirmed) {    
        consent = "accepted"
        window.open(taskurl)
        sidebar.open('home');
    }
});

var taskgen = function(){
    
}

BASECOORDS = [51.96236, 7.62571];

var TILE_URL = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
var MB_ATTR = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
mymap = L.map('llmap', {zoomDelta: 0.5,
zoomSnap: 0.5}).setView(BASECOORDS, 14);
var tile_layer = L.tileLayer(TILE_URL, {attribution: MB_ATTR}).addTo(mymap);

var transOptions = {
    radius: 4,
    fillColor: "#ff7800",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};


var amenOptions = {
    radius: 4,
    fillColor: "#00ff1e",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};

var shopOptions = {
    radius: 4,
    fillColor: "yellow",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};

var highlightPro = {
    fillColor: "red",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 1
};

var highlightVote = {
    fillColor: "yellow",
    color: "#000",
    weight: 1,
    opacity: 1,
    fillOpacity: 1
};

var normal = {
    fillColor: "blue",
    color: "#000",
    weight: 0.6,
    opacity: 1,
    fillOpacity: 0.6
};

var normalvote = {
    fillColor: "blue",
    color: "#000",
    weight: 0.6,
    opacity: 1,
    fillOpacity: 0.6
};

var Fid;

function onFeatureGroupClick(e) {
    var group = e.target,
        layer = e.layer;
  
    group.setStyle(normal);
    layer.setStyle(highlight);
  }

let selected = false
var clickedF = {}
var clickedFP = {}

var ParkLayer = L.geoJson(null, {onEachFeature: function(feature, layer){  
        layer.on('click', function() {
            Fid = feature.properties.id;
            poly = layer.getBounds().getCenter();
            selected = !selected
            lati = poly.lat;
            long = poly.lng;
            clickedF['id'] = Fid;
            clickedF['lat'] = lati;
            clickedF['lon'] = long;
            ParkLayer.setStyle(normal); //resets layer colors
            layer.setStyle(highlightPro);
            var loadpro = document.getElementById('processingwait')
            loadpro.style.display = 'none'
            var disppro = document.getElementById('processshow')
            disppro.style.display = 'block'
        });
}
});

var TranLayer = L.geoJson(null, {pointToLayer: function (feature, latlng) {
    return L.circleMarker(latlng, transOptions);
}
});
var AmLayer = L.geoJson(null, {pointToLayer: function (feature, latlng) {
    return L.circleMarker(latlng, amenOptions);
}
});
var ShopLayer = L.geoJson(null, {pointToLayer: function (feature, latlng) {
    return L.circleMarker(latlng, shopOptions);
}
});
var parkpLayer = L.geoJson(null, {onEachFeature: function(feature, layer){  
    layer.on('click', function() {
        Fid = feature.properties.id;
        poly = layer.getBounds().getCenter();
        clickedFP['id'] = Fid;
        info(Fid)
        parkpLayer.setStyle(normalvote); //resets layer colors
        layer.setStyle(highlightVote);
    });
}
});

setTimeout(function(){
    $.getJSON('/parking').done (
    function (geojsonPark) {
        ParkingLayer = ParkLayer.addData(geojsonPark);
        ParkingLayer.setStyle(normal);
        ParkingLayer.addTo(mymap);
        return ParkingLayer
        });
    }, 800);

setTimeout(function(){
    $.getJSON('/trans').done(
    function (geojsonTrans) {
        TranLayer.addData(geojsonTrans, 
            {pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, transOptions);}
            })
        });
    }, 1300);

setTimeout(function(){
    $.getJSON('/shops').done(
        function (geojsonshops) {
            ShopLayer.addData(geojsonshops, 
                {pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, shopOptions);}
                })
            return ShopLayer
            });
        }, 1900);

setTimeout(function(){
    $.getJSON('/amen').done(
    function (geojsonAmen) {
        AmLayer.addData(geojsonAmen, 
            {pointToLayer: function (feature, latlng) {
                return L.circleMarker(latlng, amenOptions);}
            })
        return AmLayer
        });
    }, 2600);

var baseMaps = {
    "Basemap": tile_layer
};

var overlayMaps = {
    "Bus stations": TranLayer,
    "Amenities": AmLayer,
    "Shops": ShopLayer,
};

var legend = L.control({ position: "bottomright" });

legend.onAdd = function(mymap) {
  var div = L.DomUtil.create("div", "legend");
  div.innerHTML += "<h4>Legend</h4>";
  div.innerHTML += '<i style="background: #ff7800; border:3px solid"></i><span>Bus stations</span><br>';
  div.innerHTML += '<i style="background: #00ff1e; border:3px solid"></i><span>Amenities</span><br>';
  div.innerHTML += '<i style="background: yellow; border:3px solid"></i><span>Shops</span><br>';
  div.innerHTML += '<i style="background: blue; border:3px solid; fillOpacity:0.6"></i><span>Parking spaces</span><br>';

  return div;
};

legend.addTo(mymap);

L.Routing.control({
    routeWhileDragging: true,
    geocoder: L.Control.Geocoder.openrouteservice({
        apiKey: '5b3ce3597851110001cf6248bb33daaee6c343659083f137bc8db889'
    }),
    router: L.Routing.mapbox('sk.eyJ1IjoiYWt1ZGVrYXIiLCJhIjoiY2t2MHNucHdpMWN5NjJ1cXd4ZWp5OHZ5ayJ9.4B1jmA1FyaqivwXvTqTqIQ'),
    showAlternatives: true,
    autoRoute: true,
    lineOptions: [{color: 'red', opacity: 1, weight: 2}],
    autocomplete: true,
}).addTo(mymap);

L.control.layers(baseMaps, overlayMaps, {collapsed: false, hideSingleBase: true}).addTo(mymap);
L.control.scale().addTo(mymap);

var clicked;
var poly;
var lati;
var long;
var latlong;

var sidebar = L.control.sidebar({
    autopan: true,       // whether to maintain the centered map point when opening the sidebar
    closeButton: true,    // whether t add a close button to the panes
    container: 'sidebar', // the DOM container or #ID of a predefined sidebar container that should be used
    position: 'left',     // left or right
}).addTo(mymap)

var gauge = new JustGage({
    id: "container", // the id of the html element
    value: score,
    min: 0,
    max: 1,
    decimals: 2,
    gaugeWidthScale: 0.8,
    pointer: true,
    customSectors: {
        percents: true, // lo and hi values are in %
        ranges: [{
            color : "#f50000",
            lo : 0,
            hi : 20
            },
            {
            color : "#ff8503",
            lo : 21,
            hi : 40
            },
            {
            color : "#ffd903",
            lo : 41,
            hi : 60
            },
            {
            color : "#c4ff03",
            lo : 61,
            hi : 80
            },
            {
            color : "#20ff03",
            lo : 81,
            hi : 100
            }]
    },
    pointerOptions: {
        toplength: null,
        bottomlength: null,
        bottomwidth: null,
        stroke: 'none',
        stroke_width: 0,
        stroke_linecap: 'square',
        color: '#000000',
    }
});

setInterval(() => {
    gauge.refresh(score);
  }, 200)

var scoreinfo;

var gauge2 = new JustGage({
    id: "container2", // the id of the html element
    value: scoreinfo,
    min: 0,
    max: 1,
    decimals: 2,
    gaugeWidthScale: 0.8,
    pointer: true,
    customSectors: {
        percents: true, // lo and hi values are in %
        ranges: [{
            color : "#f50000",
            lo : 0,
            hi : 20
            },
            {
            color : "#ff8503",
            lo : 21,
            hi : 40
            },
            {
            color : "#ffd903",
            lo : 41,
            hi : 60
            },
            {
            color : "#c4ff03",
            lo : 61,
            hi : 80
            },
            {
            color : "#20ff03",
            lo : 81,
            hi : 100
            }]
    },
    pointerOptions: {
        toplength: null,
        bottomlength: null,
        bottomwidth: null,
        stroke: 'none',
        stroke_width: 0,
        stroke_linecap: 'square',
        color: '#000000',
    }
});

setInterval(() => {
    gauge2.refresh(scoreinfo);
  }, 200)

tippy('#amen', {
    content: '<p style="font-size:20px;">Higher importance for barbers, post depots, ATMs, etc.</p>',
    allowHTML: true,
  });
tippy('#shops', {
    content: '<p style="font-size:20px;">Higher importance for bakery, grocery stores, ice cream shops, etc</p>',
    allowHTML: true,
  });
tippy('#pt', {
    content: '<p style="font-size:20px;">Higher importance for public tranport</p>',
    allowHTML: true,
  });
tippy('#walk', {
    content: '<p style="font-size:20px;">For example, to a shop you want to visit or to a bus station? The distance is in meters. The value defaults to 50 m if no input is given</p>',
    allowHTML: true,
});
tippy('#score', {
    content: '<p style="font-size:20px;">Higher score means the parking space has a lot of services (amenities, shops, bus stations) within your given walking radius and is considered a good space for a new charging station.</p>',
    allowHTML: true,
});
tippy('#weights', {
    content: '<p style="font-size:20px;">A higher percent value for shops means shops are more important to you than bus stations or other amenities</p>',
    allowHTML: true,
});

tippy('#type', {
    content: '<p style="font-size:18px;"><p>The type of charger you want to be installed:</p><ul><li><h4>Type 1</h4><p>Approx. <u>4 - 8 kms</u> range in <u>1 hr</u></p></li><li><h4>Type 2</h4><p>Approx. <u>16 - 120 kms</u> range in <u>1 hr</u> based on the charging capacity of the station and the car</p></li>'
    +' <li><h4>Type 3</h4><p>Approx. <u>4 - 38 kms</u> of range in <b><u>1 minute</u></b> based on the station and car capacity</p></li></ul></p>',
    allowHTML: true,
});

tippy('#avg', {
    content: '<p style="font-size:20px;">The average score is calculated based on what other users have processed for that parking space.</p>',
    allowHTML: true,
});
  
var userInput;
  

var sliders = $("#slider1, #slider2, #slider3");
var availableTotal = 100;


var values = []

sliders.each(function() {
    var init_value = parseInt($(this).text());
    $(this).siblings('.value').text(init_value);

    $(this).empty().slider({
        value: init_value,
        min: 0,
        max: 80,
        range: 100,
        step: 1,
        animate: 1,
        slide: function(event, ui) {
            
            // Update display to current value
            $(this).siblings('.value').text(ui.value);

            // Get current total
            var total = 0;

            sliders.not(this).each(function() {
                total += $(this).slider("option", "value");
            });

            // Need to do this because apparently jQ UI
            // does not update value until this event completes
            total += ui.value;

            var delta = availableTotal - total;

            // Update each slider
            sliders.not(this).each(function() {
                var t = $(this),
                    value = t.slider("option", "value");


                var new_value = value + (delta/2);
                
                if (new_value < 0 || ui.value == 100) 
                    new_value = 0;
                if (new_value > 100) 
                    new_value = 100;
                if (ui.value + new_value > 100)
                    alert("Please keep all sliders below 90%")
                
                t.siblings('.value').text(new_value);
                t.slider('value', new_value); 

            });
        }
    });
});

var valuesChanged = {};

$("#slider1").on( "slidechange", function( event, ui ) {
    valuesChanged["am"] = ui.value / 100
} );
$("#slider2").on( "slidechange", function( event, ui ) {
    valuesChanged["sh"] = ui.value / 100
} );
$("#slider3").on( "slidechange", function( event, ui ) {
    valuesChanged["ta"] = ui.value / 100
} );

var score;
var circle;
var processing;
var loading;

function processF() {
    processing = document.getElementById('buttonprocess')
    processing.style.display = 'none'
    loading = document.getElementById('buttonload')
    loading.style.display = 'block'
    userInput = document.getElementById("userInput").value;
    if (userInput > 500) {
        userInput = 500
        alert("please keep walking distance below 500 m")
    }
    else {
        userInput = document.getElementById("userInput").value;
    }
    fid = clickedF["id"]
    lat = clickedF["lat"]
    lon = clickedF["lon"]
    if (jQuery.isEmptyObject(Fid)) {
        alert("Please select a space to begin")
    }
    else {
        circle = L.circle([lat, lon], {radius: userInput})
        circle.setStyle({
            color: 'yellow'
        });
        circle.addTo(mymap)
        zoom = 17
        mymap.setView([lat, lon], zoom)
        var sh_found;
        var tr_found;
        var am_found;
        var x
        $.get("/process?id="+fid+"&dist="+userInput, function(data) {
            sh_found = data["shop_found"]
            am_found = data["amen_found"]
            tr_found = data["tran_found"]
            document.getElementById("amfound").innerHTML = am_found
            document.getElementById("shfound").innerHTML = sh_found
            document.getElementById("ptfound").innerHTML = tr_found
            document.getElementById("dist").innerHTML = userInput
            document.getElementById("dist1").innerHTML = userInput
            document.getElementById("dist2").innerHTML = userInput

        });
        var valuesBefore = [];
        valuesBefore.push({key: "1", value: 0.6});
        valuesBefore.push({key: "2", value: 0.3});
        valuesBefore.push({key: "3", value: 0.1});
        var aw;
        var sw;
        var tw;
        if (jQuery.isEmptyObject(valuesChanged)) {
            aw = valuesBefore[0].value;
            sw = valuesBefore[1].value;
            tw = valuesBefore[2].value;
        }
        else {
            aw = valuesChanged["am"];
            sw = valuesChanged["sh"];
            tw = valuesChanged["ta"];
        }
        
        setTimeout(function(){
            $.get("/score?id="+Fid+"&aw="+aw+"&tw="+tw+"&sw="+sw).done (
                function(datascore) {
                    score = parseFloat(datascore)
                    return datascore
            })
            loading.style.display = 'none'
            processing.style.display = "block"
        }, 3000);
      }
}


$("#voting").click(function(event) {
    event.preventDefault();
    if(mymap.hasLayer(ParkLayer)) {
        $(this).removeClass('selected');
        $.getJSON('/parkp').done (
            function (geojsonParkp) {
                ParkingPLayer = parkpLayer.addData(geojsonParkp);
                ParkingPLayer.setStyle(normalvote) 
                return ParkingPLayer
        });
        mymap.removeLayer(ParkLayer);
        mymap.addLayer(parkpLayer);
    } 
    else {
        mymap.addLayer(parkpLayer);        
        $(this).addClass('selected');
    }
    if (mymap.hasLayer(circle)) {
        mymap.removeLayer(circle);
    }
});

$("#processing").click(function(event) {
    event.preventDefault();
    if(mymap.hasLayer(parkpLayer)) {
        $(this).removeClass('selected');
        mymap.removeLayer(parkpLayer);
        mymap.addLayer(ParkLayer);
    } 
    else {
        mymap.addLayer(ParkLayer);        
        $(this).addClass('selected');
    }
    if (mymap.hasLayer(circle)) {
        mymap.removeLayer(circle);
    }
});

var moreinfo;

var scoreinfolist = [];
var info = function(fidinfo) {
    $.getJSON("/info?id="+fidinfo, function(data) {
        moreinfo = data
        for (let index = 0; index < data.length; index++) {
            const feature = data[index];
            var proper = feature["properties"];
            scoreinfolist.push(parseFloat(proper["score"]))
        }
        if (scoreinfolist.length > 1) {
            const avg = arr => arr.reduce((acc,v) => acc + v) / arr.length;
            scoreinfo = avg(scoreinfolist)
        }
        else {
            scoreinfo = scoreinfolist[0]
        }
    })
    scoreinfolist = []
}

var clearvote = function() {
    $('#r').each(function() {
        $(this).remove();
    })
} 

var more = function() {
    if (document.getElementById("target2") !== null) {
        document.getElementById("target2").remove()
    }
    $('#target').append('<div id="target2" style="height:500px; overflow-y:auto;""></div>')
    for (let index = 0; index < moreinfo.length; index++) {
        const feature = moreinfo[index];
        var proper = feature["properties"];
        $('#target2').append('<p><div style="border:1px solid black; width: 250px; padding: 5px; font-size: 15px; font-weight:bold; border-radius: 15px; background-color:rgba(0, 116, 217, 0.3); color:black;" class="ansbox"><p>walking distance: '+proper["dist"]+'m</p><p>score: '+proper["score"]+'</p><p>Amenities found: '+proper["a_f"]+'</p><p>Shops found: '+proper["s_f"]+'</p><p>Bus stations found: '+proper["t_f"]+'</p><p>Votes: '+proper["vote"]+'</p></div></p>').trigger("create")
    }
}

var votedids = [];

var vote = function() {
    if (votedids.includes(Fid)) {
        Swal.fire({  
            title: 'You already voted for this parking space',
            icon: 'error',
            confirmButtonText: `Okay`,  
          }); 
    }
    else {
        votedids.push(Fid)
        typech = document.getElementById("typech").value
        url = '/vote?id='+Fid+'&type='+typech
        if (jQuery.isEmptyObject(Fid)) {
            Swal.fire({  
                title: 'Please select a parking space',
                icon: 'error',
                confirmButtonText: `Okay`,  
            });
        }
        else {
            fetch(url)
            Swal.fire({  
                title: 'You have voted for the selected space successfully',
                icon: 'success',
                confirmButtonText: `Okay`,  
            });
        }
    }
}

var tour = function() {
    sidebar.close()
    var enjoyhint_instance = new EnjoyHint({
        onEnd:function(){
            sidebar.open('home')
        },
        onSkip: function(){
            sidebar.open('home')
        }
    });

    var enjoyhint_script_steps = [
        {
            'click .leaflet-routing-geocoders' : '<p id = "step1">Use this to display a route </p><p id = "step1">(for example, your usual route from home to work).</p><p id = "step1">Click on [+] to add more waypoints</p><p id = "step1">The navigations also shows alternative routes. Be sure to check them too</p>',
            "showNext": true,
            "arrowColor": "#FF0000"
        },
        {
            'click .leaflet-control-layers leaflet-control-layers-expanded leaflet-control' : '<p id = "step1">Use this to switch on or off</p><p id = "step1">the services you want to see on the map.</p><p id = "step1">This panel is useful for exploring the area around your selected parking space</p>',
            "showNext": true
        },
        {
            'click .home' : '<p id = "step1">Click here to get more information on the entire tool</p>',
            "showNext": true,
            "shape": "circle"
        },
        {
            'click .processing' : '<p id = "step1">Switch to this panel to calculate site suitability for a parking space.</p><p id = "step1">Begin by selecting a parking space on the map</p><p id = "step1">All steps are equipped with an information panel</p><p id = "step1">Hover over the (?) next to the text</p>',
            "showNext": true,
            "shape": "circle"
        },
        {
            'click .voting' : '<p id = "step1">Switch to this panel </p><p id = "step1">to vote on others parking spaces</p>',
            "showNext": true,
            "shape": "circle"
        }         
    ];

    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);

    //run Enjoyhint script
    enjoyhint_instance.run();
}

var prevZoom = mymap.getZoom();

var interactions2 = []

mymap.on('zoomend',function(e){

	var currZoom = mymap.getZoom();
    var diff = prevZoom - currZoom;
    var stamp = Date.now()

    if(diff > 0){
        interactions2.push({"event_name": "mapZoomOut", "event_time":stamp, "from": prevZoom.toFixed(2), "to": currZoom.toFixed(2)});
    } else if(diff < 0) {
        interactions2.push({"event_name": "mapZoomIn", "event_time":stamp, "from": prevZoom.toFixed(2), "to": currZoom.toFixed(2)});
    }
    prevZoom = currZoom;
});

var movestartcenter;
var movetime;

mymap.on('movestart', function(e){
    movetime = Date.now()
    movestartcenter = mymap.getCenter()
})

mymap.on('moveend', function(e){
    var moveendcenter = mymap.getCenter()
    interactions2.push({"event_name":"pan", "event_time": movetime,"from": movestartcenter, "to": moveendcenter});
})

mymap.on("click", function (e) {
    var stamp = Date.now()

    interactions2.push({"event_name": "mapclick", "event_time":stamp})
});


$('.buttonvote').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name": "voteclick", "event_time":stamp})
});

$('.buttonprocess').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'btnprocessclick', "event_time":stamp})
});

$('#tour').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'btntourclick', "event_time":stamp})
});

$('#voting').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'panelvoteclick', "event_time":stamp})
});

$('#processing').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'panelprocessclick', "event_time":stamp})
});

$('#home').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'panelhomeclick', "event_time":stamp})
});

$('#help1').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'help1click', "event_time":stamp})
});

$("#slider1").on("slidechange", function() {
    var stamp = Date.now()

    interactions2.push({"event_name":'slider1change', "event_time":stamp})
});

$("#slider2").on("slidechange", function() {
    var stamp = Date.now()

    interactions2.push({"event_name":'slider2change', "event_time":stamp})
});

$("#slider3").on("slidechange", function() {
    var stamp = Date.now()

    interactions2.push({"event_name":'slider3change', "event_time":stamp})
});

$('.leaflet-routing-geocoder').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'routingclick', "event_time":stamp})
});

$('.leaflet-routing-add-waypoint').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'addwaypointclick', "event_time":stamp})
});

$('.leaflet-control-layers-list').click(function () {
    var stamp = Date.now()

    interactions2.push({"event_name":'switchlayersclick', "event_time":stamp})
});


var donetesting = function() {
    Swal.fire({  
        title: 'Are you sure you have finished testing',
        icon: 'question',
        confirmButtonText: `Yes`,
        showCancelButton: true,
        cancelButtonText: "No",
        allowOutsideClick: false, 
      }).then((result) => { 
          if (result.isConfirmed) {    
            if (consent == "accepted"){
                var interactions = [{"id": sessionid, "data":interactions2}];
                $.ajax({
                    url: "/clicklog?sessid=" + sessionid + "&condition=" + consent,
                    type: "POST",
                    contentType: 'text/plain',
                    dataType:"text", 
                    data: JSON.stringify(interactions),
                });
                window.open("/que?id=" + sessionid, '_blank').focus();
            }
            else {
                var interactions = {}
            }
          }
    });

}
