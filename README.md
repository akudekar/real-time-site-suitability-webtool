# real-time-site-suitability-webtool

This web-based PPGIS is designed for my master thesis and is aimed at helping making residents of Münster make an informed decision and participate in the process of deciding for new locations for potential E-charging stations. This is a [Flask](https://flask.palletsprojects.com/en/2.0.x/) application with a [PostgreSQL](https://www.postgresql.org/) database and a pure JS/HTML/CSS front end.

Suitability algorithm - geo-weighted sum. Any other formula can be applied in the backend with sql scripts.

![Home panel](./images/home.PNG)

![Walkthrough-wizard](./images/4.PNG)

![suitability panel](./images/3.PNG)

![Voting panel](./images/7.PNG)
