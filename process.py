#processing script returning the count of features inside defined buffer for site suitability

def addFidGeom(cursor, fid, dist):
    insert_sql = f"insert into tmp_table(fid, geom) select osm_id, geom FROM parking where parking.osm_id = '{fid}'"
    dist_sql = f"update tmp_table set dist = {dist}; "
    cursor.execute(insert_sql)
    cursor.execute(dist_sql)

def process(cursor, layer, dist, fid):
    buffer_sql = f"select * from parking, {layer} where ST_DWithin(public.parking.geom, public.{layer}.geom, {dist}) and parking.osm_id='{fid}';"
    cursor.execute(buffer_sql)
    countFeatures = []
    for row in cursor.fetchall():
        countFeatures.append(row)
    count_int = len(countFeatures)
    return count_int

def populate(cursor, fid, v1, v2, v3, count_int1, count_int2, count_int3):
    count_sql_s = f"update tmp_table set {v1} = {count_int1}"
    count_sql_t = f"update tmp_table set {v2} = {count_int2}"
    count_sql_a = f"update tmp_table set {v3} = {count_int3}"
    cursor.execute(count_sql_s)
    cursor.execute(count_sql_t)
    cursor.execute(count_sql_a)

def sqlfunction(fid, value, cursor, v):
    cursor1 = cursor
    sql = f"update tmp_table set {v} = {value}"
    cursor1.execute(sql)
