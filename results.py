from process import process, sqlfunction
from geojson import Feature, FeatureCollection, loads
import decimal

class buffer(object):
    def __init__(self, cursor, Fid, dist) -> None:
        self.cursor = cursor
        self.Fid = Fid
        self.dist = dist
        super().__init__()
    
    def shop_results(self, ):
        Fid = self.Fid
        cursor = self.cursor
        dist = self.dist
        layer = "shops"
        v1 = "s_v"
        featureCountShops = process(cursor, layer, dist, Fid)
        if featureCountShops <= 3:
            value = "0.25"
            sqlfunction(Fid, value, cursor, v1)
        elif 3 <= featureCountShops <= 6:
            value = "0.5"
            sqlfunction(Fid, value, cursor, v1)
        elif 6 <= featureCountShops <= 9:
            value = "0.75"
            sqlfunction(Fid, value, cursor, v1)
        elif featureCountShops > 9:
            value = "1"
            sqlfunction(Fid, value, cursor, v1)
        return featureCountShops
        
    def tran_results(self, ):
        Fid = self.Fid
        cursor = self.cursor
        dist = self.dist
        layer = "public_transport"
        v1 = "t_v"
        featureCountTrans = process(cursor, layer, dist, Fid)
        if featureCountTrans <= 3:
            value = "0.25"
            sqlfunction(Fid, value, cursor, v1)
        elif featureCountTrans > 3 & featureCountTrans <= 6:
            value = "0.5"
            sqlfunction(Fid, value, cursor, v1)
        elif 6 <= featureCountTrans <= 9:
            value = "0.75"
            sqlfunction(Fid, value, cursor, v1)
        elif featureCountTrans > 9:
            value = "1"
            sqlfunction(Fid, value, cursor, v1)
        return featureCountTrans

    def amen_results(self, ):
        Fid = self.Fid
        cursor = self.cursor
        dist = self.dist
        layer = "amenities"
        v1 = "a_v"
        featureCountAmen = process(cursor, layer, dist, Fid)
        if featureCountAmen <= 3:
            value = "0.25"
            sqlfunction(Fid, value, cursor, v1)
        elif 3 <= featureCountAmen <= 6:
            value = "0.5"
            sqlfunction(Fid, value, cursor, v1)
        elif 6 <= featureCountAmen <= 9:
            value = "0.75"
            sqlfunction(Fid, value, cursor, v1)
        elif featureCountAmen > 9:
            value = "1"
            sqlfunction(Fid, value, cursor, v1)
        return featureCountAmen

class scores(object):
    def final(cursor, tw, sw, aw, fid) -> FeatureCollection:
        sqlCalc = f"update tmp_table set score = ((s_v * {sw})+(a_v * {aw})+(t_v * {tw}))"
        sqlVote = f"update tmp_table set vote = 0"
        cursor = cursor
        cursor.execute(sqlCalc)
        cursor.execute(sqlVote)
        sqlDisp = f"SELECT score::float from tmp_table;"  # type: list
        cursor.execute(sqlDisp)
        sc = cursor.fetchone()
        sc = str(sc)
        final_insert = "insert into parking_p(fid, geom, dist, a_f, s_f, t_f, a_v, s_v, t_v, score, vote) select fid, geom, dist, a_f, s_f, t_f, a_v, s_v, t_v, score, vote FROM tmp_table;"
        cursor.execute(final_insert)
        return (str(sc[1])+str(sc[2])+str(sc[3])+str(sc[4]))
        
            