import sys
from flask import Flask, render_template, jsonify, send_from_directory, request
from display import Postgis
import geojson
import gspread
import random
from google.oauth2 import service_account
from googleapiclient.discovery import build

app = Flask(__name__)
port = 5000
app.secret_key = "Lrez1s2ak"

credential = service_account.Credentials.from_service_account_file("/home/akudekar/flask/credentials.json")
cred = credential.with_scopes(["https://www.googleapis.com/auth/spreadsheets"])
service = build('sheets','v4', credentials=cred)

db = Postgis('postgres', 'postgress', 5432, '172.18.0.3')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/help1.html')
def help1():
    return render_template('help1.html')

@app.route('/help2.html')
def help2():
    return render_template('help2.html')

@app.route('/help3.html')
def help3():
    return render_template('help3.html')

@app.route('/help4.html')
def help4():
    return render_template('help4.html')

@app.route('/parking')
def parking():
    park = db.get_parking()
    return geojson.dumps(park)

@app.route('/parkp')
def parking_p():
    park_p = db.get_processed()
    return geojson.dumps(park_p)

@app.route('/amen')
def amen():
    amenit = db.get_amenities()
    return geojson.dumps(amenit)

@app.route('/shops')
def shop():
    shop = db.get_shops()
    return geojson.dumps(shop)

@app.route('/trans')
def tran():
    transp = db.get_trans()
    return geojson.dumps(transp)

@app.route('/process')
def FinalProcess():
    pro = db.processing()
    return pro

@app.route('/score')
def FinalScore():
    scor = db.scoresDisp()
    return scor

@app.route('/info')
def info() -> None:
    vote = db.infor()
    return vote

@app.route('/vote')
def finalvote() -> None:
    vote = db.voteAdd()
    return vote

@app.route('/clicklog', methods=['GET', 'POST'])
def clicklog() -> None:
    click = db.clicklog()
    return click

@app.route("/consent")
def consent():
    return render_template('consent.html')

@app.route("/statres")
def statres():
    return render_template('statres.html')

@app.route('/all_reviews', methods=["GET"])
def all_reviews():
    headers = gsheet.get()
    enumerated_headers = list(enumerate(headers))   
    return jsonify(enumerated_headers)

@app.route("/tasks")
def tasks():
    ssid = request.args.get("ssid")
    homelist = ["Gescherweg", "Heekweg", "Stickamp", "Westenkamp", "Weseler Straße", "Albersloher Weg", "Mondstrasse", "Dieckmanstraße", "Glückweg", "Willy-brandt-weg", "Coerdestiege"]
    worklist = ["Frauenstraße", "Domplatz", "Alter Steinweg", "Hamburgerstraße", "Metzer straßé", "Bonhoefferstrasse"]
    hadd = random.choice(homelist)
    wadd = random.choice(worklist)
    return render_template('tasksheet.html', sessid=ssid, source=hadd, work=wadd)

@app.route('/que', methods=["GET", "POST"])
def questions():
    global sessid
    if request.method == "GET":
        sessid = request.args.get("id")
        return render_template('forms.html')
    elif request.method == "POST":
        answerjson = {}
        data = request.get_data()
        answers = data.decode().split("&")
        for i in answers:
            spl = i.split("=")
            answerjson[spl[0]] = spl[1]
        evalinsert = db.inserteval(sessid, answerjson)
        return data

@app.route('/evalresult')
def evalresult():
    res = db.evalresultdisp(service)
    return(res)

@app.route('/clickresult')
def clickresult():
    res = db.clickresultdisp()
    return(res)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=port, threaded=True, ssl_context='adhoc', debug=True)
